export function declOfNum (number, titles) {
    const cases = [2, 0, 1, 1, 1, 2];
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
}

export function rowGenerator (numberOfRows) {
    let rows = [];
    for (let i = 1; i < numberOfRows; i++) {
        rows.push({
            id: i,
            task: 'Task ' + i,
            complete: Math.min(100, Math.round(Math.random() * 110)),
            priority: ['Critical', 'High', 'Medium', 'Low'][Math.floor((Math.random() * 3) + 1)],
            issueType: ['Bug', 'Improvement', 'Epic', 'Story'][Math.floor((Math.random() * 3) + 1)]
        });
    }
    return rows;
}