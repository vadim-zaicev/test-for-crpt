import * as React from "react";
import * as ReactDOM from "react-dom";
import {Provider} from "react-redux";
import { StoreHelper } from "./helpers/StoreHelper";

import App from "../components/App/App";

const mountApp = function () {
    console.log('mount app...');

    const store = StoreHelper.createStore();

    window['store'] = store;

    const container = document.querySelector('#react-app-container');

    if (container) {
        ReactDOM.render(
            <Provider store={store}>
                <App/>
            </Provider>,
            container
        );
    }

};

export { mountApp };