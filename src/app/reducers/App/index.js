import { combineReducers } from 'redux';

import {columns} from "./columns";
import {rows} from "./rows";

export default combineReducers({
    columns,
    rows
});