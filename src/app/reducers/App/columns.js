import {constants} from "../../actions/constants";

export function columns(state = [], action = {}) {
    switch(action.type) {

        case constants.APP.COLUMNS.LOADED:
            return action.value.slice();

        default:
            return state;
    }
}