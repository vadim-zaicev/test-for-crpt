import {constants} from "../../actions/constants";

export function rows(state = [], action = {}) {
    switch(action.type) {

        case constants.APP.ROWS.UPDATE_INVOKED:
            if (action.value.fromRow !== action.value.toRow) return state;

            const column = state[action.value.fromRow];

            return [
                ...state.slice(0, action.value.fromRow),
                Object.assign({}, column, action.value.updated),
                ...state.slice(action.value.fromRow + 1)
            ];

        case constants.APP.ROWS.LOADED:
            return action.value.slice();

        case constants.APP.ROWS.REMOVE_INVOKED:
            console.log('invoked', action.value);
            const removeIndex = state.findIndex(r => r.id === action.value.id);

            return [
                ...state.slice(0, removeIndex),
                ...state.slice(removeIndex + 1)
            ];

        default:
            return state;
    }
}