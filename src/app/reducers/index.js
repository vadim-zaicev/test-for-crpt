import { combineReducers } from 'redux';
import App from "../reducers/App";

export default combineReducers({
    App,
});