export class ColumnNormalizer {

    static forReactTable(columns, params) {
        return columns.map(column => {

            const newColumn = {};

            newColumn.Header = column.name;
            newColumn.accessor = column.key;

            if (~params.editables.indexOf(newColumn.accessor)) {
                newColumn.Cell = params.Cell;
            }

            return newColumn;
        });
    }

}