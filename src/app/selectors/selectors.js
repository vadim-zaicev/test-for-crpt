import { createSelector } from 'reselect';

export function getRows(state) {
    return state.App.rows;
}

export function getColumns(state) {
    return state.App.columns;
}