import {rowGenerator} from "../utils/index";

export class DataService { //ммммаксимально нейтральное название сервиса, поскольку неизвестна предметная область

    static getInitialData() {

        return new Promise((resolve, reject) => {

            const rows = rowGenerator(500);

            resolve({
                columns: [
                    {
                        key: 'id',
                        name: 'ID',
                    },
                    {
                        key: 'task',
                        name: 'Title',
                        editable: true
                    },
                    {
                        key: 'priority',
                        name: 'Priority',
                        editable: true
                    },
                    {
                        key: 'issueType',
                        name: 'Issue Type',
                        editable: true
                    },
                    {
                        key: 'complete',
                        name: '% Complete',
                        editable: true
                    }
                ],
                rows: rows
            });

        });

    }

}