import { constants } from "./constants";

export function rowsLoaded(value) {
    return {
        type: constants.APP.ROWS.LOADED,
        value,
    }
}

export function columnsLoaded(value) {
    return {
        type: constants.APP.COLUMNS.LOADED,
        value,
    }
}

export function rowUpdateInvoked(value) {
    return {
        type: constants.APP.ROWS.UPDATE_INVOKED,
        value
    }
}

export function rowRemoveInvoked(value) {
    return {
        type: constants.APP.ROWS.REMOVE_INVOKED,
        value
    }
}