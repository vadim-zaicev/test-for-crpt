const constants = {
    APP: {

        ROWS: {
            LOADED: null,
            UPDATE_INVOKED: null,
            REMOVE_INVOKED: null,
        },

        COLUMNS: {
            LOADED: null,
        }

    }
};

function updateKey(obj, key, value) {

    if (obj[key] === null) {
        return value;
    }

    if (typeof obj[key] === 'string') {
        return obj[key];
    }

    const answer = {};

    for (const innerKey in obj[key]) {
        answer[innerKey] = updateKey(obj[key], innerKey, value + '_' + innerKey);
    }

    return answer;

}

for (const key in constants) {
    constants[key] = updateKey(constants, key, key);
}

export { constants };