import * as React from "react";

import ReactDataGrid from "react-data-grid";


export function EditableTable(props) {

    const {
        rows,
        minHeight,
        enableCellSelect,
        columns,
        rowGetter,
        rowsCount,
        onGridRowsUpdated,
        rowHeight,
        onRemoveRow,
    } = props;


    return <ReactDataGrid
            enableCellSelect={enableCellSelect !== undefined ? enableCellSelect : true}
            minHeight={minHeight !== undefined ? minHeight : 320}
            rowGetter={rowGetter ? rowGetter : i => rows[i]}
            rowsCount={rowsCount !== undefined ? rowsCount : (rows ? rows.length : 0)}
            columns={columns ? columns : []}
            rowHeight={rowHeight !== undefined ? rowHeight : 40}
            rows={rows ? rows : []}
            onGridRowsUpdated={onGridRowsUpdated ? onGridRowsUpdated : null}
            getCellActions={(column, row) => {
                if (column.key === 'id') {
                    return [
                        {
                            icon: 'fa fa-times pointer',
                            callback: onRemoveRow ? () => onRemoveRow(column, row) : function(){}
                        },
                    ];
                }
            }}
        />
}