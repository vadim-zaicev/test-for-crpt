import * as React from "react";
import { connect } from "react-redux";

import classNames from "classnames/bind";

import {EditableTable} from "../EditableTable/";
import {EditableTable2} from "../EditableTable2/";

import { columnsLoaded, rowsLoaded, rowUpdateInvoked, rowRemoveInvoked } from "../../app/actions/actionCreators";
import { getColumns, getRows } from "../../app/selectors/selectors";
import {DataService} from "../../app/services/DataService";

class App extends React.Component {

    componentWillMount() {

        DataService.getInitialData()
            .then(data => {
                this.props.columnsLoaded(data.columns);
                this.props.rowsLoaded(data.rows);
            });

    }


    render() {

        return <div>

            <EditableTable
                rows={this.props.rows}
                columns={this.props.columns}
                onGridRowsUpdated={params => this.props.rowUpdateInvoked(params)}
                onRemoveRow={(column, row) => confirm(`Точно хотите удалить строку ID${row.id}?`) ? this.props.rowRemoveInvoked(row) : null}
            />

            <EditableTable2
                columns={this.props.columns}
                editables={this.props.columns.map(c => c.key)}
                rows={this.props.rows}
                Cell={cellInfo => <input
                    value={cellInfo.original[cellInfo.column.id]}
                    onChange={e => {
                        this.props.rowUpdateInvoked({
                            fromRow: cellInfo.index,
                            toRow: cellInfo.index,
                            updated: {
                                [cellInfo.column.id]: e.target.value
                            }
                        });
                    }}
                />}
            />


        </div>;
    }

}

const mapStateToProps = (state, props) => {


    return {
        rows: getRows(state),
        columns: getColumns(state),
    };
};


export default connect(mapStateToProps, {
    columnsLoaded,
    rowsLoaded,
    rowUpdateInvoked,
    rowRemoveInvoked,
})(App);

