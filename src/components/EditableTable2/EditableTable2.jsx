import * as React from "react";
import ReactTable from "react-table";
import {ColumnNormalizer} from "../../app/normalizers/ColumnNormalizer";


export function EditableTable2(props) {

    const {
        columns,
        Cell,
        editables,
        rows,
    } = props;

    const normalizedColumns = ColumnNormalizer.forReactTable(columns, {Cell, editables});

    return <div style={{marginTop: "20px"}}>
        <ReactTable
            columns={normalizedColumns}
            data={rows}
            defaultPageSize={10}
            className="-striped -highlight"
        />
    </div>
}